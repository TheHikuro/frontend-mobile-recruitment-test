import {axiosInstance} from '../../utils/axios';
import {AxiosResponse} from 'axios';
import {Note} from '../models/notes';

export const getAllNotes = (): Promise<AxiosResponse<Note[]>> =>
  axiosInstance.get('/api/notes');

export const getOneNote = (params: {
  id: string;
}): Promise<AxiosResponse<Note>> =>
  axiosInstance.get('/api/notes/:id', {params});

// TODO implement the query using react-query
// export const useGetAllNotes = () => {};

// TODO implement the query using react-query
// export const useGetOneNote = () => {}
