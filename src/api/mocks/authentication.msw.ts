import {rest} from 'msw';
// import {CORRECT_EMAIL, CORRECT_PASSWORD} from '../../utils/constants';

export const getAuthenticationMSW = () => [
  rest.post<{email?: string; password?: string}>(
    'http://mock.api/api/login',
    (req, res, ctx) => {
      if (!req.body) {
        return res(ctx.delay(50), ctx.status(401));
      }

      // if (
      //   req.body?.email !== CORRECT_EMAIL ||
      //   req.body?.password !== CORRECT_PASSWORD
      // ) {
      //   return res(ctx.delay(50), ctx.status(401));
      // }
      return res(ctx.delay(100), ctx.status(200, 'Mocked status'));
    },
  ),
  rest.post('http://mock.api/api/logout', (req, res, ctx) => {
    return res(ctx.status(200, 'Mocked status'));
  }),
];
