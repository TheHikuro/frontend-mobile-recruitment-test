import {
  Button as RnUiLibButton,
  ButtonProps as RnUiLibButtonProps,
  Colors,
} from 'react-native-ui-lib';
import React from 'react';

type ButtonProps = RnUiLibButtonProps;

export const Button = (props: ButtonProps) => {
  return <RnUiLibButton backgroundColor={Colors.$iconPrimary} {...props} />;
};
