import {Colors, Text} from 'react-native-ui-lib';
import React, {FC} from 'react';

export const FieldError: FC = ({children}) => {
  return (
    <Text text80 color={Colors.$textDanger}>
      {children}
    </Text>
  );
};
