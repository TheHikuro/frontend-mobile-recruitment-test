# Frontend Mobile Recruitment Test

Welcome 👋

As a mobile developer you will have to implement new features in apps and fix bugs.\
The goal of this project is to test both skills.

You will need to first implement new features.\
You will achieve this by creating new components, adding new functions but also re-use existing code.\
In parallel, you'll also need fix some issues that will pop up in the existing code.

Read this carefully then take a look in `src`, there are readmes in each folder with a quick summary of its content.

> App should function on both **Android** and **iOS**.

# The App

This is a simple app to display a list of notes and their details in a separate screen, the user can also log in and
logout.

Your implementation should follow the look of the screenshots.\
If you are ambitious and have some time left you could implement a custom theme and make the app prettier.

> For the ui, use [react-native-ui-lib](https://wix.github.io/react-native-ui-lib/) and [styled-components](https://styled-components.com/)

There are ``// TODO`` and ``// FIXME`` comments in the code that shows where there's something to change/fix.\
As an extra-credit, you can improve on any logic you see or asked to implement in the next sections (Making the app more
secure, prettier, ect...)

> The project has a set structure, screens and ui components are in specific folders. This is on purpose.

## Login Screen

![Login Screen](./documentation/android/login-screen.png)

Enter a correct email and password to login.

Each field is validated and errors are displayed this way:

![Field Validation](./documentation/android/field-validation.png)

### Requirements

Use [react-navigation](https://reactnavigation.org/) to handle navigation between screens

Use [react-hook-form](https://react-hook-form.com/) for the login form and validate fields
using [yup](https://github.com/jquense/yup):

- Email field should be a correct email and is required
- Password field is required
- Fields are validated on blur, changes and submission

After implementing the form, for the login logic, take a look into ``src/utils/context/auth.context.tsx``.\
There are multiple issues in that file, fix it to continue.

> Users should only log in once, when re-opening the app, check the user has logged in and redirect to the home screen

## Home Screen

After login, user is redirected to the home screen:

![Home Screen](./documentation/android/home-screen.png)

This is a scrollable screen that displays a list of notes fetched from the server.

Pressing the `Logout` button will log out the user and redirect to the login screen

> You can use [Shopify's flash-list lib](https://shopify.github.io/flash-list/) if you finish early

## Note Details Screen

Pressing a card in the home screen redirect to the note's details screen:

![Note Details Screen](./documentation/android/note-details-screen.png)

Pressing the back button returns the user to the previous page

---

Good luck 👍
